let trainers = {};
let tableParentId = 'table';
let detailsParentId = 'details';

function getRowId(resource) {
    return `${resource.id}_row`
}

function updateDetails(trainerId) {
    const trainer = trainers?.data?.find(item => item.id === trainerId);
    const parent = document.getElementById(detailsParentId);

    parent.innerHTML = `
    <div class="card" id="${trainer.id}_card" xmlns="http://www.w3.org/1999/html">
      <img src="${trainer.profileUrl}" class="card-img-top" alt="${trainer.name}">    
      <div class="card-body">
        <h5 class="card-title">${trainer.name}</h5>
        <p class="card-text">
            ID: ${trainer.id} </br>
            Name: ${trainer.name} </br>
            Created: ${trainer.created} </br>
            Last Updated: ${trainer.lastUpDate}
        </p>
      </div>
    </div>
    `
}

function trainerToRow(trainer) {
    return `
    <tr id="${getRowId(trainer)}" onclick="updateDetails('${trainer.id}')">
        <th scope="row">${trainer.id}</th>
        <td>${trainer.name}</td>
        <td>${trainer.created}</td>
        <td>${trainer.lastUpDate}</td>
    </tr>
    `
}

function createTrainersTable() {
    const tableParent = document.getElementById(tableParentId);
        tableParent.innerHTML = `
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th scope="col">ID</th>
                        <th scope="col">Name</th>
                        <th scope="col">Created</th>
                        <th scope="col">Last Updated</th>
                    </tr>
                </thead>
                <tbody>
                ${
                    trainers.data.map(resource => `${trainerToRow(resource)}`).join("\n")
                    || "no data"
                }
                </tbody>    
            </table>
        `

}